<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'PagesController@home');

Route::get('/guasap', function () {
    return view('guasap');
});

Route::get('/messages/{message}', 'MessagesController@show');

Route::get('/auth/facebook', 'SocialAuthController@facebook');
Route::get('/auth/facebook/callback', 'SocialAuthController@callback');
Route::post('/auth/facebook/register', 'SocialAuthController@register');

Route::group(['middleware' => 'auth'], function() {
    Route::post('/{username}/dms', 'UsersController@sendPrivateMessage');
    Route::post('/messages/create', 'MessagesController@create');
    Route::get('/conversations/{conversation}', 'UsersController@showConversation');
    Route::post('/{username}/follow', 'UsersController@follow');
    Route::post('/{username}/unfollow', 'UsersController@unfollow');
});

Route::get('/{username}/follows', 'UsersController@follows');
Route::get('/{username}/followers', 'UsersController@followers');
Route::get('/{username}', 'UsersController@show');