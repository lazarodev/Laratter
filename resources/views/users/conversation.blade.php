@extends('layouts.app')

@section('content')

<h1>Conversacion con {{ $conversation->users->except($user->id)->implode('name', ', ') }}</h1>

<div class="card">
@foreach($conversation->privateMessages as $message)
<div class="card">
<div class="card-header">
    <p>{{ $message->user->name }} dijo...</p>
    <p>
    <div class="card-block">
    {{ $message->message }}
    </div>
    </p>
    <p>
     {{ $message->created_at }}
     </p>
</div>

@endforeach
</div>
@endsection